# This module contains small simple but useful functions that convert strings, manage weird cases. Its purpose is to bring more readability into the code of the main functions
from numpy import log10


def logarithm(x):
    """
    We assume no town has a population that is not an integer. But some towns in the database can have 0 inhabitants.
    We then don't want the logarithm to be infinity if this is the case, hence this small function. Similarly when x < 0 we still want to comput
    the logarithm of abs(x) and we add a - sign in front of the result

    parameters
    -------
    x       integer

    return
    -------
    the logarithme in base 10 of abs(x) or 0 signed by the sign of x
    """
    if x > 0:
        return log10(x)
    elif x < 0:
        return -log10(-x)
    else:
        return 0


def determine_code(year, info):
    """
    functions that convert year and info into the code used by the database

    parameters
    -------
    year        string
    info        string

    returns
    ------
    string containing the code
    """
    if info == "DENS":
        return "densite"
    elif info == "MIG":
        return "solde_migratoire"
    elif info == "SOLDE":
        return "solde_net"
    else:
        if year in ["2018", "2013", "2008", "1999", "1990", "1982", "1975", "1968"]:
            if int(year) < 2000:
                return "D" + year[2] + year[3] + "_" + info
            else:
                return "P" + year[2] + year[3] + "_" + info

        elif year in ["1318", "0813", "9908", "9099", "8290", "7582", "6875"]:
            return info + year

        else:
            return ""


def determine_scale_code(scale):
    """
    functions that convert scale "departements" -> 'DEP', "regions" -> 'REG', "communes" -> "LIBGEO"

    parameters
    -------
    scale        string containing the scale

    returns
    ------
    string containing the scale_code
    """
    if scale == "departements":
        return "DEP"

    elif scale == "regions":
        return "REG"

    elif scale == "communes":
        return "LIBGEO"

    else:
        return ""


def determine_scale(scale_code):
    """
    functions that convert scale "DEP" -> 'departements', "REG" -> 'regions', "LIBGEO" -> "communes"

    parameters
    -------
    scale        string containing the scale_code

    returns
    ------
    string containing the scale
    """
    if scale_code == "REG":
        return "regions"
    if scale_code == "DEP":
        return "departements"
    if scale_code == "LIBGEO":
        return "communes"
    else:
        return ""


def convert_date(period_input):
    """
    The users choose a periode eg. '2013-2018' but this corresponds to '1318' in the data base. This function does the conversion

    parameters
    ----------
    period_input        string          eg. '2013-2018'


    returns
    ----------
    string                              eg. '1318'
    """
    y1 = period_input[0] + period_input[1]
    y2 = period_input[2] + period_input[3]

    def f(y):
        if y in ["99", "90", "82", "75", "68"]:
            return "19" + y
        else:
            return "20" + y

    return [f(y1), f(y2)]
