# load and convert databases into a dictionnary with the towns INSEE IDs as indexes for the dataframe
import pandas as pd


def load_csv_database_dataframe(filename, index_column):
    """
    Convert a CSV file into a Dataframe object

    Parameters
    ----------
    filename:       string
            relative path from this python file to the CSV file
    index_column:   int
            index of the column of the database we want to use as index
            for the dataframe

    Returns
    -------
    df:             panda dataframe

    Example
    -------
    filename1 = 'data/db_recensement_1968à2018.csv'
    index_column1 = 0
    df1 = load_csv_database_dataframe(filename1,index_column1)
    """
    df = pd.read_csv(filename, sep=";",
                     index_col=index_column, low_memory=False)
    return df


def merge_df(list_df):
    """
    Merges a list of panda dataframes. Simply a reformulation of the
    concatenation build-in function

    Parameters
    ----------
    list_df:    list
        list of dataframes

    Returns
    -------
                panda dataframe
    """
    return pd.concat(list_df, axis=1)


filename1 = "data/db_recensement_1968à2018.csv"
index_column1 = 0
df_recensement = load_csv_database_dataframe(filename1, index_column1)

filename2 = "data/villes_spatiales.csv"
index_column2 = 0
df_spatiale = load_csv_database_dataframe(filename2, index_column2)

df_merged = merge_df([df_recensement, df_spatiale])
