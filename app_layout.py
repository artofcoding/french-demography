# Layout of the app

import dash_core_components as dcc
import dash_html_components as html

color = 'rgb(0, 64, 128)'
b_color = 'rgb(0,26,17)'


def layout(available_cities, available_datas, fig1, fig2, fig3, fig4, fig5):
    return html.Div([
        html.Div([
            html.Div(children=[
                    html.H1(children='Étude démographique par ville'),

                    html.Br(),

                    html.Div('Sélectionnez la ville à étudier : '),

                    dcc.Dropdown(
                        id='city-input',
                        options=[{'label': i, 'value': i}
                                 for i in available_cities],
                        value='Paris',
                        style={'color': color}
                    ),


                    html.Div('Sélectionnez la donnée à étudier : '),

                    dcc.Dropdown(
                        id='data-input',
                        options=[{'label': i, 'value': i}
                                 for i in available_datas["Données"]],
                        value='Population',
                        style={'color': color}
                    ),

                    html.Br(),

                    html.H2(id='city-output'),

                    html.Br(),

                    html.Div([
                        html.Div([
                            html.H3(id='data-output'),
                            dcc.Graph(id='city-graph', figure=fig1)
                        ], style={'display': 'flex', 'flex-direction': 'column', 'justify-content': 'space-between', 'width': 700}),
                        html.Div([
                            html.H3(
                                'Répartition des logements dans cette ville'),
                            html.Div([
                                html.H5('Sélectionnez l\'année :'),
                                dcc.Dropdown(
                                    id='year5-input',
                                    options=[{'label': i, 'value': i} for i in [
                                        '2018', '2013', '2008', '1999', '1990', '1982', '1975', '1968']],
                                    value='2018',
                                    style={'color': color, 'width': 200})
                            ], style={'display': 'flex', 'flex-direction': 'row', 'margin': 10, 'padding': 15, 'justify-content': 'space-between'}),
                            dcc.Graph(id='fig5', figure=fig5)
                        ], style={'width': 600})
                    ], style={'display': 'flex', 'flex-direction': 'row', 'justify-content': 'space-around', 'margin': 15, 'padding': 10})
                    ], style={'backgroundColor': color, 'margin': 15, 'padding': 10, 'borderRadius': 15})
        ], style={}),
        html.Div([
            html.Div(children=[

                html.Div([
                    html.H1(
                        children='Cartes des dynamiques démographiques françaises'),

                    html.Br(),

                    html.Div('Sélectionnez la donnée à étudier :'),

                    dcc.Dropdown(
                        id='info-input',
                        options=[{'label': 'Population', 'value': 'POP'},
                                 {'label': 'Logement', 'value': 'LOG'},
                                 {'label': 'Résidence principale', 'value': 'RP'},
                                 {'label': 'Résidence secondaire',
                                     'value': 'RSECOCC'},
                                 {'label': 'Logement vacants', 'value': 'LOGVAC'},
                                 {'label': 'Population en résidence principale',
                                  'value': 'PMEN'},
                                 {'label': 'Densité de population', 'value': 'DENS'}],
                        value='POP',
                        style={'color': color}
                    ),

                    html.Br(),

                    html.Div('Sélectionnez l\'échelle :'),

                    dcc.Dropdown(
                        id='scale-input',
                        options=[{'label': 'régionale', 'value': 'REG'},
                                 {'label': 'départementale', 'value': 'DEP'},
                                 # {'label': 'communes', 'value': 'LIBGEO'}
                                 ],
                        value='DEP',
                        style={'color': color}
                    ),

                    html.Br(),

                    html.Div('Sélectionnez l\'année :'),

                    dcc.Dropdown(
                        id='year-input',
                        options=[{'label': i, 'value': i} for i in [
                            '2018', '2013', '2008', '1999', '1990', '1982', '1975', '1968']],
                        value='2018',
                        style={'color': color}
                    )
                ], style={'display': 'flex', 'flex-direction': 'column'}),

                html.Div([
                    html.H2(id='all-output'),

                    dcc.Graph(
                        id='map',
                        figure=fig2
                    )
                ], style={'display': 'flex', 'flex-direction': 'column'})
            ], style={'display': 'flex', 'flex-direction': 'column', 'justify-content': 'space-between', 'padding': 15, 'margin': 10, 'backgroundColor': color, 'borderRadius': 15, 'width': 800}),
            html.Div(children=[

                html.Div([
                    html.H1(
                        children='Naissances, décès et migrations en France'),

                    html.Br(),

                    html.Div('''Sélectionnez la donnée à étudier :'''),

                    dcc.Dropdown(
                        id='info2-input',
                        options=[{'label': 'Naissance', 'value': 'NAIS'},
                                 {'label': 'Décès', 'value': 'DECE'},
                                 {'label': 'Migration', 'value': 'MIG'},
                                 {'label': 'Solde naturel', 'value': 'SOLDE'}
                                 ],
                        value='NAIS',
                        style={'color': color}
                    ),

                    html.Br(),

                    html.Div('''Sélectionnez l'échelle :'''),

                    dcc.Dropdown(
                        id='scale2-input',
                        options=[{'label': 'régionale', 'value': 'REG'},
                                 {'label': 'départementale', 'value': 'DEP'},
                                 # {'label': 'communes', 'value': 'LIBGEO'}
                                 ],
                        value='DEP',
                        style={'color': color}
                    ),

                    html.Br(),

                    html.Div('''Sélectionnez l'année :'''),

                    dcc.Dropdown(
                        id='period-input',
                        options=[{'label': '20' + i[0:2] + '-' + '20' + i[2:4], 'value': i} for i in ['1318', '0813']] + [{'label': "1999-2008",
                                                                                                                           'value': '9908'}] + [{'label': '19' + i[0:2] + '-' + '19' + i[2:4], 'value': i} for i in ['9099', '8290', '7582', '6875']],
                        value='1318',
                        style={'color': color}
                    )
                ], style={'display': 'flex', 'flex-direction': 'column'}),

                html.Div([
                    html.H2(id='all2-output'),

                    dcc.Graph(
                        id='map2',
                        figure=fig3
                    )
                ], style={'display': 'flex', 'flex-direction': 'column'})
            ], style={'display': 'flex', 'flex-direction': 'column', 'justify-content': 'space-between', 'padding': 15, 'margin': 10, 'backgroundColor': color, 'borderRadius': 15, 'width': 800})
        ], style={'display': 'flex', 'flex-direction': 'row', 'color': 'white'}),

        html.Div(children=[

            html.H1('Répartition de la population en fonction de l\'altitude'),

            html.Br(),

            dcc.Graph(
                id='fig4',
                figure=fig4
            ),

            html.Div('Sélectionnez l\'année : '),

            dcc.Dropdown(
                id='year4-input',
                options=[{'label': i, 'value': i} for i in [
                    '2018', '2013', '2008', '1999', '1990', '1982', '1975', '1968']],
                value='2018',
                style={'color': color}
            ),
        ], style={'padding': 15, 'margin': 10, 'backgroundColor': color, 'borderRadius': 15})
    ], style={'fontFamily': 'Arial', 'color': 'white', 'backgroundColor': b_color, 'margin-bottom': 0})
