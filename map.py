# This module provides a plot.ly figure ready to be implemented in the dash interface
import plotly.express as px
import json

from statistics import fetch_data
from utils import logarithm, determine_code, determine_scale_code


def fetch_geo_data(scale):
    """
    function that create a variable containing the geographical data in the right format to be used by choropleth

    parameters
    -------
    scale           either 'regions', 'departements'

    return
    -------
    dictionnary containing the geographic data
    """
    scale_code = determine_scale_code(scale)

    # Loading geojson
    path = "data_geo/" + scale + ".json"
    with open(path) as f:
        geo_world = json.load(f)

    # Instanciating necessary lists
    scale_geo = []

    # Looping over the custom GeoJSON file
    for scale in geo_world["features"]:

        # Departements name detection
        if scale_code == "DEP" or scale_code == "REG":
            scale_num = scale["properties"]["code"]
        else:
            scale_num = scale["properties"]["insee"]

        # Treating the weird case apart (only for the DEP)
        if scale_code == "DEP":
            if scale_num in {"01", "02", "03", "04", "05", "06", "07", "08", "09"}:
                scale_num = scale_num[1]

        # Getting information from both GeoJSON file and dataFrame
        geometry = scale["geometry"]

        # Adding 'id' information for further match between map and data
        scale_geo.append(
            {"type": "Feature", "geometry": geometry, "id": scale_num})

    # Displaying metrics
    return {"type": "FeatureCollection", "features": scale_geo}


def define_s(data, code, green_option):
    """
    Depending on the info the scale may be different, this funtions computes all the useful parameters to code the scale later in the create_map() function

    parameters
    ----------
    data            panda dataframe with the data we want on the map
    code            string                                                  eg. 'P18_POP' for population in 2018
    green_option    bool, gives a scale of colour from red to green
    log_option      bool, asks if we want to use the log for the scale

    return
    ----------
    the tuple (min_log, max_log, color_continuous_scale_v, values, ticks) with al the useful information to code the colour scale of the map
    """
    # We apply the log function to speed up data processing and make a nicer graph
    data["count_color"] = data[code].apply(logarithm)

    # Get the maximum value to cap displayed values
    max_log = data["count_color"].max()
    max_val = int(max_log) + 1

    min_log = data["count_color"].min()
    min_val = int(min_log)

    # Prepare the range of the colorbar
    values = [i for i in range(min_val, max_val + 1)]
    ticks = [-(10 ** (-i)) for i in values if i < 0] + [
        10 ** i for i in values if i > 0
    ]

    if green_option:
        color_continuous_scale_v = "RdYlGn"
    else:
        color_continuous_scale_v = "YlOrRd"

    return min_log, max_log, color_continuous_scale_v, values, ticks


def create_map(df, scale, info, year):
    """
    function that creates an interactive map with the data of our datasets about french demography

    parameters
    -----
    df          panda dataframe containing the data
    scale       there are two available maps of France, one with the regions et one with de departments, scale is
                therefore a string that contains either "region" ou "departement"

    info        select the info we want on the map. Info is a string and uses the code of the database recensement_1968a2018

    year        String containing the year.

    returns
    -----
    a plot.ly map ready to be implemented in the dash interface.
    """
    # A problem is that in the column of our database info and year are mixed in category such as P18_POP
    # We have to do small manipulation about the arguments given
    # We define a scale_code (either DEP or REG), and a code (such as P18_POP) to match the database column
    scale_code = determine_scale_code(scale)
    code = determine_code(year, info)

    # fetching the geographic data
    geo_scale_ok = fetch_geo_data(scale)

    # fetching the data
    tmp = code
    data, code = fetch_data(df, scale_code, tmp, info, year)

    # define the scale on the side of the map
    if info == "MIG" or info == "SOLDE":
        min_log, max_log, color_continuous_scale_v, values, ticks = define_s(
            data, code, True
        )
    else:
        min_log, max_log, color_continuous_scale_v, values, ticks = define_s(
            data, code, False
        )

    # Create figure
    fig = px.choropleth_mapbox(
        data,
        geojson=geo_scale_ok,
        locations=scale_code,
        color=data["count_color"],
        range_color=(min_log, max_log),
        color_continuous_scale=color_continuous_scale_v,
        hover_name=scale_code,
        hover_data={"count_color": False, scale_code: False, code: True},
        mapbox_style="open-street-map",
        zoom=4,
        center={"lat": 46.227638, "lon": 2.213749},
        opacity=0.8,
    )

    # Define layout specificities
    fig.update_layout(
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
        coloraxis_colorbar={"title": info,
                            "tickvals": values, "ticktext": ticks},
    )

    # Display figure
    return fig
