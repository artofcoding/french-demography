# Principe du projet

Dans ce projet, nous nous intéressons à un outil interactif de visualisation de données relatives à la démographie française.
Les données utilisées ont été téléchargées sur le site data.gouv.fr, elles portent sur les recensements de la population française effectués depuis 1968 jusqu’en 2018. On retrouve dans ce fichier .csv des informations concernant la population des villes, les nombres de naissance et décès, la répartition du parc immobilier municipal entre résidences principales et résidences secondaires, ou la superficie de la ville.
Notre objectif est de permettre à l’utilisateur de choisir l’information qu’il souhaite visualiser, ainsi que l’échelle administrative voulue.

# Équipe

Simon Curis, Martin Huot, Arthur Manceau, Luke Pilache, Jean-Eudes Pinczon Du Sel

# Comment contribuer

TO DO

# Comment utiliser le projet

Notre projet s'exécute depuis le terminal.

La première fois:
- Cloner le projet
```bash
git clone https://gitlab-ovh-03.cloud.centralesupelec.fr/arthur.manceau/demography_datagang
```
- Installer les packages nécessaires
```bash
pip install pandas
pip install dash
```

À chaque utilisation:
- Lancer le programme
```dash
python main.py
```
- Afficher la page
```html
etg
```
Remarque: la page prend alors quelques secondes à se charger correctement



Une fois le programme exécuté, le lien vers l’application Dash de visualisation s’affiche notamment dans le terminal. Vous pouvez aussi ouvrir le lien suivant: [http:/127.0.0.1:8050](http:/127.0.0.1:8050). Sur l’application, des menus déroulants permettent à l’utilisateur de faire varier les données représentées par défaut sur les graphes, pour choisir de voir l’évolution de la population dans sa ville préférée, ou bien de comparer le solde naturel de chaque département grâce à la carte de France interactive.

## Prérequis

Il faut installer le package python `dash` à l'aide de la commande pip associée à votre système.

## Lancer l’application

TO DO

# Les bases de données

## Description

TO DO

## Utilisation

Les modules de graphes sont basés sur des fonctions génériques de statistique. Elles leur fournissent les _valeurs_ et prennent comme argument l’_échelle géographique_, _l’entité géographique_ et le _type de valeur_ voulue. 

Ces fonctions génériques ont été construites sur des fonctions plus spécifiques à notre base de données initiale. Cette dernière est indexée par n°INSEE de commune en ligne et par _type de valeur_ en colonne, donc la clé primaire est le n°INSEE.

Au sein du module `load_convert_database`, nous avons la fonction `load_csv_database_dataframe` qui extrait le dataframe panda associé à une base de donnée sous format .csv. La fonction `merge_df` fusionne 2 bases de données sur les clés primaires de ces bases de données. L’utilisateur est donc libre d'utiliser n’importe quel ensemble de bases de données, tant qu’elles sont indexées sur les n°INSEE de commune. 

On a utilisé le document `db_recensement_1968à2018`, une base de données de recensement des communes pour tous les graphes et les applications: seule des données de cette base de donnée sont accessibles à l’utilisateur.

# Remarques à l'attention de l’utilisateur final

Pour les histogrammes sur les naissances et les décès, les valeurs dans la DB sont pour une période de temps. L’application affiche donc la valeur _proratée_ au nombre d’année pour chaque année. Visuellement cela donne des plateaux.

# A dire dans la présentation

On a pas de fichier test et de try, error. Pour le try error, on a pas besoin, l’utilisateur choisi par une liste de choix et possible

