import dash
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output

from load_convert_database import df_merged, df_recensement as df
from map import create_map
from app_layout import layout
from statistics import pop_altitude, part_logements, extract_births, extract_deaths, extract_pop, extract_vac_res, extract_principal_res, extract_sec_res, extract_lodging
from utils import determine_scale, convert_date

# Initalizing the dash app
app = dash.Dash(__name__)

# ----------------------------------------- The first figure --------------------------------------
# Some useful lists
available_cities = df["LIBGEO"]
available_datas = pd.DataFrame(
    {
        "Données": [
            "Population",
            "Naissances",
            "Décès",
            "Logements",
            "Résidences principales",
            "Résidences secondaires",
            "Logements vacants",
        ]
    }
)
years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]

# initialize the figure
df1 = extract_pop(df, "Paris")
fig1 = px.line(df1, x="Année", y="Population")

# Interaction


@app.callback(
    Output(component_id="city-output", component_property="children"),
    Input(component_id="city-input", component_property="value"),
)
def update_output_div_city(city1_input):
    """
    function that displays the city choosen by the user. Warning : this function is automatically called by the callback()

    parameters
    ----------
    scale1_input     string     eg. 'Paris'

    returns
    -----------
    title of the graph under the form of a string
    """
    return "Vous avez choisi " + city1_input


@app.callback(
    Output(component_id="city-graph", component_property="figure"),
    [
        Input(component_id="city-input", component_property="value"),
        Input(component_id="data-input", component_property="value"),
    ],
)
def update_graph(input_value_city, input_value_data):
    """
    function that update the graph. Warning : this function is automatically called by the callback()

    parameters
    ----------
    input_value_city            string
    input_value_data            string

    returns
    ---------
    a plot.ly figure updated and displayed on the app
    """
    global df
    if input_value_data == "Population":
        df1 = extract_pop(df, input_value_city)
        fig1 = px.line(df1, x="Année", y="Population")
        return fig1

    elif input_value_data == "Naissances":
        df1 = extract_births(df, input_value_city)
        fig1 = px.bar(df1, x="Année", y="Naissances")
        return fig1

    elif input_value_data == "Décès":
        df1 = extract_deaths(df, input_value_city)
        fig = px.bar(df1, x="Année", y="Décès")
        return fig

    elif input_value_data == "Logements":
        df1 = extract_lodging(df, input_value_city)
        fig = px.line(df1, x="Année", y="Logements")
        return fig

    elif input_value_data == "Résidences principales":
        df1 = extract_principal_res(df, input_value_city)
        fig = px.line(df1, x="Année", y="Résidences principales")
        return fig

    elif input_value_data == "Résidences secondaires":
        df1 = extract_sec_res(df, input_value_city)
        fig = px.line(df1, x="Année", y="Résidences secondaires")
        return fig

    elif input_value_data == "Logements vacants":
        df1 = extract_vac_res(df, input_value_city)
        fig = px.line(df1, x="Année", y="Logements vacants")
        return fig
    else:
        print("Error")


@app.callback(
    Output(component_id="data-output", component_property="children"),
    Input(component_id="data-input", component_property="value"),
    Input(component_id="city-input", component_property="value"),
)
def update_graph_data(input_value, city_input):
    """
    function that displays the city and the info choosen by the user. Warning : this function is automatically called by the callback()

    parameters
    ----------
    input_value    string     eg. 'Population'
    city_input     string     eg. 'Paris'

    returns
    -----------
    title of the graph under the form of a string
    """
    s = ""
    if input_value == "Population":
        s = "Évolution de la population dans la ville de : "
    elif input_value == "Naissances":
        s = "Évolution du nombre de naissances dans la ville de : "
    elif input_value == "Décès":
        s = "Évolution du nombre de décès dans la ville de : "
    elif input_value == "Logement":
        s = "Évolution du nombre de logements dans la ville de : "
    elif input_value == "Résidence principales":
        s = "Évolution du nombre de résidences principales dans la ville de : "
    elif input_value == "Résidences secondaires":
        s = "Évolution du nombre de résidences secondaires dans la ville de : "
    elif input_value == "Logements vacants":
        s = "Évolution du nombre de logements vacants dans la ville de : "
    else:
        print("error")

    return s + city_input


# ------------------------------------- The second figure -------------------------------------
# Initialize the figure
dfp = part_logements(df, "Paris", "2018")
l = ["Résidence principale", "Résidence secondaire", "Logement vacant"]
fig5 = px.pie(
    dfp, values="Prop", names=l, color_discrete_sequence=px.colors.sequential.dense
)

# Interaction


@app.callback(
    Output(component_id="fig5", component_property="figure"),
    [
        Input(component_id="year5-input", component_property="value"),
        Input(component_id="city-input", component_property="value"),
    ],
)
def update_fig5(year5_input, city_input):
    """
    function that update the graph. Warning : this function is automatically called by the callback()

    parameters
    ----------
    year5_input                 string          eg. '2018'
    city_input                  string          eg. 'Paris'

    returns
    ---------
    a plot.ly figure updated and displayed on the app
    """
    dfp = part_logements(df, city_input, year5_input)
    return px.pie(
        dfp, values="Prop", names=l, color_discrete_sequence=px.colors.sequential.dense
    )


# -------------------------------------- The third figure --------------------------------------
# Initialize the figure
df_ = pop_altitude(df_merged, "2018")
fig4 = px.line(df_, x="population", y="Altitude moyenne", log_y=True)


@app.callback(
    Output(component_id="fig4", component_property="figure"),
    Input(component_id="year4-input", component_property="value"),
)
def update_fig4(year4_input):
    """
    function that update the figure 4 when the user changes the year
    """
    return px.line(
        pop_altitude(df_merged, year4_input),
        x="population",
        y="Altitude moyenne",
        log_y=True,
    )


# ---------------------------------------  map 1 -----------------------------------------------
# Initialize the figure
fig2 = create_map(df, "departements", "POP", "2018")

# Interaction


@app.callback(
    Output(component_id="all-output", component_property="children"),
    [
        Input(component_id="scale-input", component_property="value"),
        Input(component_id="info-input", component_property="value"),
        Input(component_id="year-input", component_property="value"),
    ],
)
def update_output_div(scale_input, info_input, year_input):
    """
    function that displays the title of the map.  Warning : this function is automatically called by the callback()

    parameters
    ----------
    scale_input     string          eg. 'REG'
    info_input      string          eg. 'POP'
    year_input      string          eg. 2018

    returns
    -----------
    title of the graph under the form of a string
    """

    # We use dictonnary to transform the code making the link with the database into correct french sentences
    d1 = {"REG": "régionale", "DEP": "départementale", "LIBGEO": "des communes"}
    d2 = {
        "POP": "de la population",
        "LOG": "des logements",
        "RP": "des résidences principales",
        "RSECOCC": "des résidences secondaires",
        "LOGVAC": "des logements vacants",
        "PMEN": "population résidence principale",
        "DENS": "de la densité de population",
    }

    scale_v = d1[scale_input]
    info_v = d2[info_input]

    return "Répartition " + info_v + " à l'échelle " + scale_v + " en " + year_input


@app.callback(
    Output(component_id="map", component_property="figure"),
    [
        Input(component_id="scale-input", component_property="value"),
        Input(component_id="info-input", component_property="value"),
        Input(component_id="year-input", component_property="value"),
    ],
)
def update_map(scale_input, info_input, year_input):
    """
    function that update the graph. Warning : this function is automatically called by the callback()

    parameters
    ----------
    scale_input                 string      eg. 'REG'
    info_input                  string      eg. 'POP'
    year_input                  string      eg. '2018'

    returns
    ---------
    a plot.ly map updated and displayed on the app
    """
    global df

    scale = determine_scale(scale_input)

    return create_map(df, scale, info_input, year_input)


# ---------------------------------------  map 2 -----------------------------------------------
# Initialize the figure
fig3 = create_map(df, "departements", "NAIS", "1318")

# Interaction


@app.callback(
    Output(component_id="all2-output", component_property="children"),
    [
        Input(component_id="scale2-input", component_property="value"),
        Input(component_id="info2-input", component_property="value"),
        Input(component_id="period-input", component_property="value"),
    ],
)
def update_output_div2(scale2_input, info2_input, period_input):
    """
    function that displays the title of the map.  Warning : this function is automatically called by the callback()

    parameters
    ----------
    scale2_input     string     eg. 'REG'
    info2_input      string     eg. 'POP'
    period_input     string     eg. '1318'

    returns
    -----------
    title of the graph under the form of a string
    """

    # We use dictonnary to transform the code making the link with the database into correct french sentences
    d1 = {"REG": "régionale", "DEP": "départementale", "LIBGEO": "des communes"}
    d2 = {
        "NAIS": "des naissances par habitants",
        "DECE": "des décès par habitants",
        "SOLDE": "du solde naturel",
        "MIG": "des migrations",
    }

    scale_v = d1[scale2_input]
    info_v = d2[info2_input]

    return (
        "Répartition "
        + info_v
        + " à l'échelle "
        + scale_v
        + " de "
        + convert_date(period_input)[0]
        + " à "
        + convert_date(period_input)[1]
    )


@app.callback(
    Output(component_id="map2", component_property="figure"),
    [
        Input(component_id="scale2-input", component_property="value"),
        Input(component_id="info2-input", component_property="value"),
        Input(component_id="period-input", component_property="value"),
    ],
)
def update_map2(scale2_input, info2_input, period_input):
    """
    function that update the graph. Warning : this function is automatically called by the callback()

    parameters
    ----------
    scale_input                 string      eg. 'REG'
    info_input                  string      eg. 'POP'
    year_input                  string      eg. '2018'

    returns
    ---------
    a plot.ly map updated and displayed on the app
    """
    global df

    scale = determine_scale(scale2_input)

    return create_map(df, scale, info2_input, period_input)


# App Layout
app.layout = layout(available_cities, available_datas,
                    fig1, fig2, fig3, fig4, fig5)


# Launching the app when the file main.py is executed
if __name__ == "__main__":
    app.run_server(debug=True)
