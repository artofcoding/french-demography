# Contains functions that transform the database to be plotted in the dashboard

from pandas.core.frame import DataFrame
from data.db_dict_id import columns_id, list_events
from load_convert_database import df_recensement as df
from load_convert_database import df_merged
import pandas as pd

from utils import convert_date, determine_code

# ------------------------------------ Get the data already in the database ready ---------------------------------------


def data_transform(df, scale_code, code):
    """
    function that format the data to be plotted on the map. Note this function is only useful for info directly in the database.

    parameters
    --------
    df              panda dataframe
    scale_code      either 'REG', 'DEP' or 'LIBGEO'
    code            corresponding column name of the dataframe

    returns
    -------
    a dataframe composed of scale_code subitems and the data of the wanted info for each
    """
    return df.groupby(scale_code)[code].sum().reset_index()


# ---------------------------------------- Calculated parameters -------------------------------------------


def dens_pop(df, scale_code, dens_annee):
    """
    function that returns a new dataframe containing for each subitem of scale the density of population

    parameters
    ---------
    df              panda dataframe
    scale_code      either 'REG', 'DEP' or 'COM'
    dens_annee      string containing the year. The format is a bit odd (see examples)       eg. 'dens_18' for the year 2018

    returns
    --------
    a panda dataframe
    """
    # 1: on récupère le nom de la colonne de df correspondant à la densite grâce aux dictionnaires de db_dict_id
    colonne1 = columns_id[list_events[dens_annee][1]][0][0]
    # on charge un nouveau df avec les colonnes voulues
    nouveau_df = df.loc[:, [colonne1, "SUPERF", scale_code]]

    # on recupère les df correspondants à la pop et à la surface group by echelle
    nouveau_df2 = pd.DataFrame(
        {"densite": nouveau_df.groupby(scale_code)[colonne1].sum()}
    ).reset_index()
    nouveau_df3 = pd.DataFrame(
        {"surface": nouveau_df.groupby(scale_code)["SUPERF"].sum()}
    ).reset_index()
    # on calcule la densité
    nouveau_df2["densite"] = nouveau_df2["densite"] / nouveau_df3["surface"]
    return nouveau_df2


def solde_net(df, scale_code, period):
    """
    function that returns a new dataframe containing for each subitem of scale the net rate

    parameters
    ---------
    df              panda dataframe
    scale_code      either 'REG', 'DEP' or 'COM'
    period          string containing the periode       eg. '1318' for 2013-2018

    returns
    --------
    a panda dataframe
    """
    # recuperation des colonnes utiles du df
    annees_naissances = "nai_" + period
    colonne1 = columns_id[list_events[annees_naissances][0]][0][0]
    annees_deces = "dec" + annees_naissances[3:]
    colonne2 = columns_id[list_events[annees_deces][0]][0][0]

    nouveau_df1 = pd.DataFrame(
        {"solde_net": df.groupby(scale_code)[colonne1].sum()}
    ).reset_index()
    nouveau_df2 = pd.DataFrame(
        {"deces": df.groupby(scale_code)[colonne2].sum()}
    ).reset_index()
    nouveau_df1["solde_net"] = nouveau_df1["solde_net"] - nouveau_df2["deces"]
    return nouveau_df1


def immigration(df, scale_code, period):
    """
    function that returns a new dataframe containing for each subitem of scale the immigration

    parameters
    ---------
    df              panda dataframe
    scale_code      either 'REG', 'DEP' or 'COM'
    period          string containing the periode       eg. '1318' for 2013-2018

    returns
    --------
    a panda dataframe
    """
    annee_naissance1 = "pop_" + period[2:4]
    annee_naissance2 = "pop_" + period[0:2]
    colonne1 = columns_id[list_events[annee_naissance1][0]][0][0]
    colonne2 = columns_id[list_events[annee_naissance2][0]][0][0]
    nouveau_df1 = pd.DataFrame(
        {"solde_migratoire": df.groupby(scale_code)[colonne1].sum()}
    ).reset_index()
    nouveau_df2 = pd.DataFrame(
        {"solde_migratoire": df.groupby(scale_code)[colonne2].sum()}
    ).reset_index()
    nouveau_df1["solde_migratoire"] = (
        nouveau_df1["solde_migratoire"] - nouveau_df2["solde_migratoire"]
    )
    nouveau_df1["solde_migratoire"] = (
        nouveau_df1["solde_migratoire"] -
        solde_net(df, scale_code, period)["solde_net"]
    )
    return nouveau_df1


def normalize_per_inhabitants(df, scale_code, code, info, year):
    data = stat_map(df, scale_code, code, info, year)

    year1, year2 = convert_date(year)

    code1, code2 = determine_code(year1, "POP"), determine_code(year2, "POP")

    pop1, pop2 = stat_map(df, scale_code, code1, "POP", year1), stat_map(
        df, scale_code, code2, "POP", year2
    )

    data["perh"] = [
        data[code][k] / ((pop1[code1][k] + pop2[code2][k]) / 2)
        for k in range(len(data))
    ]

    return data, "perh"


# ---------------------------------------


def stat_map(df, scale_code, code, info, year):
    """
    function that returns the appropriate statistical function to compute on the map the info wanted

    parameters
    -------
    df              panda dataframe
    scale_code      either 'REG', 'DEP' or 'COM'
    code            string corresponding column name of the dataframe
    info            string containing the info wanted

    returns
    --------
    the appropriate statistical function
    """
    if info == "DENS":
        return dens_pop(df, scale_code, "dens_" + year[2] + year[3])
    if info == "MIG":
        return immigration(df, scale_code, year)
    if info == "SOLDE":
        return solde_net(df, scale_code, year)
    else:
        return data_transform(df, scale_code, code)


def fetch_data(df, scale_code, code, info, year):
    # In the case of birth and death we want to compare to the total of inhabitants
    if info == "NAIS" or info == "DECE":
        return normalize_per_inhabitants(df, scale_code, code, info, year)
    else:
        return stat_map(df, scale_code, code, info, year), code


# ------------------------------------- Functions for the first figure ----------------------------------


def extract_pop(df, city):
    """
    function that returns a new dataframe containing population rates for the city 'city'.

    parameters
    ---------
    df              panda dataframe
    city            string containing the name of the city
    returns
    --------
    a panda dataframe
    """
    years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]
    pop = df.loc[df["LIBGEO"] == city]
    pop = pop.iloc[:, 0:8]
    L = []
    for i in pop:
        L += [pop[i][0]]
    return pd.DataFrame({"Année": years, "Population": L})


def extract_births(df, city):
    """
    function that returns a new dataframe containing birth rates for the city 'city'.

    parameters
    ---------
    df              panda dataframe
    city            string containing the name of the city
    returns
    --------
    a panda dataframe
    """
    years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]
    births = df.loc[df["LIBGEO"] == city]
    births = births.iloc[:, 9:16]
    L = []
    j = 0
    for i in births:
        L += [births[i][0]] * (years[j] - years[j + 1])
        j += 1
    return pd.DataFrame(
        {"Année": [1968 + i for i in range(2018 - 1968)], "Naissances": L}
    )


def extract_deaths(df, city):
    """
    function that returns a new dataframe containing death rates for the city 'city'.

    parameters
    ---------
    df              pandas dataframe
    city            string containing the name of the city
    returns
    --------
    a panda dataframe
    """
    years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]
    deaths = df.loc[df["LIBGEO"] == city]
    deaths = deaths.iloc[:, 16:23]
    L = []
    j = 0
    for i in deaths:
        L += [deaths[i][0]] * (years[j] - years[j + 1])
        j += 1
    return pd.DataFrame({"Année": [1968 + i for i in range(2018 - 1968)], "Décès": L})


def extract_lodging(df, city):
    """
    function that returns a new dataframe containing lodging rates for the city 'city'.

    parameters
    ---------
    df              pandas dataframe
    city            string containing the name of the city
    returns
    --------
    a panda dataframe
    """
    years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]
    lodg = df.loc[df["LIBGEO"] == city]
    lodg = lodg.iloc[:, 23:31]
    L = []
    for i in lodg:
        L += [lodg[i][0]]
    return pd.DataFrame({"Année": years, "Logements": L})


def extract_principal_res(df, city):
    """
    function that returns a new dataframe containing principal residence rates for the city 'city'.

    parameters
    ---------
    df              pandas dataframe
    city            string containing the name of the city
    returns
    --------
    a panda dataframe
    """
    years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]
    princ_res = df.loc[df["LIBGEO"] == city]
    princ_res = princ_res.iloc[:, 31:39]
    L = []
    for i in princ_res:
        L += [princ_res[i][0]]
    return pd.DataFrame({"Année": years, "Résidences principales": L})


def extract_sec_res(df, city):
    """
    function that returns a new dataframe containing secondary residence rates for the city 'city'.

    parameters
    ---------
    df              pandas dataframe
    city            string containing the name of the city
    returns
    --------
    a panda dataframe
    """
    years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]
    sec_res = df.loc[df["LIBGEO"] == city]
    sec_res = sec_res.iloc[:, 39:47]
    L = []
    for i in sec_res:
        L += [sec_res[i][0]]
    return pd.DataFrame({"Année": years, "Résidences secondaires": L})


def extract_vac_res(df, city):
    """
    function that returns a new dataframe containing vacant residence rates for the city 'city'.

    parameters
    ---------
    df              pandas dataframe
    city            string containing the name of the city
    returns
    --------
    a panda dataframe
    """
    years = [2018, 2013, 2008, 1999, 1990, 1982, 1975, 1968]
    vac_res = df.loc[df["LIBGEO"] == city]
    vac_res = vac_res.iloc[:, 47:55]
    L = []
    for i in vac_res:
        L += [vac_res[i][0]]
    return pd.DataFrame({"Année": years, "Logements vacants": L})


# ------------------------------------------ Fonction for the second figure -------------------------------------------------


def part_logements(df, scale_id, year, scale_code="LIBGEO"):
    """
    functions that calculates the proportion of main residence, secondary residence and available housing units.
    It will be used for the second figure.

    parameters
    ---------
    df              pandas dataframe
    scale_id        string containing the insee code of a town or the number of the region, departement
    year            string containing the year                                                                By default : 2018
    scale_code      either 'REG', 'DEP' or 'LIBGEO'. Note that we don't implement this function to work with another parameter than 'LIBGEO'

    returns
    ---------
    a triplet of numbers        (prop_res_p, prop_res_s, prop_log_v)
    """
    if year in ["2018", "2008", "2013"]:
        token = "P"
    else:
        token = "D"

    log_p = token + year[2] + year[3] + "_RP"
    log = token + year[2] + year[3] + "_LOG"
    log_s = token + year[2] + year[3] + "_RSECOCC"
    log_v = token + year[2] + year[3] + "_LOGVAC"
    line = pd.DataFrame(df.loc[df[scale_code] == scale_id])
    nb_log = int(line[log])
    nb_log_p = int(line[log_p])
    nb_log_s = int(line[log_s])
    nb_log_v = int(line[log_v])
    prop_log_p = nb_log_p / nb_log
    prop_log_s = nb_log_s / nb_log
    prop_log_v = nb_log_v / nb_log
    return pd.DataFrame({"Prop": [prop_log_p, prop_log_s, prop_log_v]})


# ------------------------------------------ Fonction for the third figure -------------------------------------------------


def pop_altitude(df_merged, annee):
    """[A function that calculates the population who lives at each altitude]

    Args:
        df_merged ([pandas dataframe]): [df_merged imported from load_convert]
        annee ([string]): [the year (for example '2018')]

    Returns:
        [pandas dataframe]: [one column gives population and the other gives the altitude]
    """
    colonne1 = columns_id[list_events["pop_" + annee[2:]][0]][0][0]

    nouveau_df1 = df_merged.loc[:, ["Altitude min", "Altitude max", colonne1]]
    nouveau_df1["Altitude min"] = (
        nouveau_df1["Altitude min"] + nouveau_df1["Altitude max"]
    ) / 2
    nouveau_df1 = pd.DataFrame(
        {"population": nouveau_df1.groupby("Altitude min")[colonne1].sum()}
    ).reset_index()
    nouveau_df1.rename(
        columns={"Altitude min": "Altitude moyenne"}, inplace=True)
    return nouveau_df1
